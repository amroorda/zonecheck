#!/bin/sh

ExitCode=0

for zonefile in tests/fail/*.zone
do
  ./do-test.sh ${zonefile}
  result=$?
  if [ ${result} -ne 1 ] ; then
    echo \* Test failed for ${zonefile}
    ExitCode=1
  else
    echo + Test passed for ${zonefile}
  fi
done

for zonefile in tests/pass/*.zone
do
  ./do-test.sh ${zonefile}
  result=$?
  if [ ${result} -ne 0 ] ; then
    echo \* Test failed for ${zonefile}
    ExitCode=1
  else
    echo + Test passed for ${zonefile}
  fi
done

if [ ${ExitCode} -ne 0 ] ; then
  echo
  echo
  echo ****
  echo *
  echo * Not all tests passed. Please check the output above.
  echo *
  echo ****
fi

exit ${ExitCode}
