#
# We do need to know what file to load

my ( $FileName );

$FileName = $ARGV[0];

if ( ! defined $FileName ) {
  print "usage: zonecheck [-c cachezise] [-d domainname] [-h <refresh_min>,<refresh_max>,<retry_min>,<retry_max>,<expire_min>,<expire_max>,<minimum_min>,<minimum_max>,<minimum_negative_min>,<minimum_negative_max>] [-msSV] zonefile\n";
  exit 1;
  }

#
# If no domain name is set on the command line
# then we use the filename as domain name

if ( $domain eq "" ) {
  $_ = (fileparse($FileName))[0];
  $domain = $_;
  }

#
# The domain should end in "."

if ( ! ( $domain =~ /\.$/o ) ) {
  $domain .= ".";
  }

$GlobalDomain = uc ( $domain );

# $DomainList { $domain } = $domain;

open INPUT, "$ARGV[0]" or die $FileName . ": No such file or directory\n";

#
# Taking the input lines, and parsing them into a standard format
# is split up in 2 routines.
#
# NormalizeInput: Takes an input line, and snips off any unneeded
# whitespace. It also snips comments and "$INCLUDE" and "$GENERATE"
# statements.
#
# NormalizeInput2: Makes sure that all elements are present. This
# means it now has a TTL, CLASS, RR, and options to the RR.
#
# Once both have been completed we are left with a standardized
# input line, and we can now disect it and pass it to the required
# RR check.
#
# BEWARE: Each RR check can _ONLY_ return 1 error / warning, and
# the first _FATAL_ error should immediately return.
#
# We also keep track of the various RR's found for the host entries
# so we can do some smarter checks later on.

$number = 0;

MainLoop: while ( <INPUT> ) {
  $number++;
  chomp;
  $Input = $_;
  $Line = NormalizeInput ( $Input );

  $InputLine { $number } = $Input;

  if ( $Line eq "" ) {
    delete $InputLine { $number };
    next MainLoop;
    }
  else {
    if ( $Line =~ /\(/o ) {
      $number2 = $number;
      $Buffer = $Line;
      next MainLoop if ( ! ( $Buffer =~ /\)/o ) );
      }
    elsif ( ( $Buffer =~ /\(/o ) && ( ! ( $Buffer =~ /\)/o ) ) ) {
      $Buffer .= " " . $Line;
      $number_of_lines_joined++;
      next MainLoop if ( ! ( $Buffer =~ /\)/o ) );
      $_ = $Buffer;
      s/[\(\)]//og;
      $Buffer = $_;
      }
    else {
      $number2 = $number;
      $Buffer = $Line
      }
    $number = $number2;

    $_ = $Buffer;

    if ( $_ =~ /^\$ORIGIN/o ) {
      /^\$ORIGIN\s+(\S+)/o;
      $domain = $1;
      $number += $number_of_lines_joined;
      $number_of_lines_joined = 0;
      next MainLoop;
      }

    if ( $_ =~ /^\$TTL\s+\S+$/o ) {
      /^\$TTL\s+(\S+)$/o;
      if ( defined ConvertAge ( uc $1 ) ) {
        $TTLbyDirective = ConvertAge ( uc $1 );
        }
      $number += $number_of_lines_joined;
      $number_of_lines_joined = 0;
      next MainLoop;
      }

    if ( defined $opt_v ) {
      print "---" . $number . "- -" . $_ . "\n";
      }

    $Buffer = NormalizeInput2 ( $Buffer );
    $_ = $Buffer;

    if ( defined $opt_v ) {
      print "- -" . $number . "- -" . $_ . "\n";
      }

    if ( $_ =~ /^(\S+)\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $2;
      $ttl = ConvertAge ( uc $3 );
      $rr = $5;
      $rest = $6;
      if ( defined $opt_v ) {
        print "* 1 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $2;
      $ttl = $3;
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 2 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o ) {
      /^\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o;
      $class = $1;
      $ttl = ConvertAge ( uc $2 );
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 3 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o ) {
      /^\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o;
      $class = $1;
      $ttl = uc $2;
      $rr = $3;
      $rest = $4;
      if ( defined $opt_v ) {
        print "* 4 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $4;
      $ttl = ConvertAge ( uc $2 );
      $rr = $5;
      $rest = $6;
      if ( defined $opt_v ) {
        print "* 5 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $3;
      $ttl = $2;
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 6 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $class = $3;
      $ttl = ConvertAge ( uc $1 );
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 7 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $class = $2;
      $ttl = $1;
      $rr = $3;
      $rest = $4;
      if ( defined $opt_v ) {
        print "* 8 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+(\S+)\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $2;
      $ttl = "";
      $rr = $3;
      $rest = $4;
      if ( defined $opt_v ) {
        print "* 9 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^\s+(\S+)\s+(\S+)\s+(.*)/o;
      $class = $1;
      $ttl = "";
      $rr = $2;
      $rest = $3;
      if ( defined $opt_v ) {
        print "* 10-" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    else {
      if ( defined $opt_v ) {
        print "* 11-" . $_ . "\n";
        }
      LogError ( $FileName, "Invalid input", 1, $number );
      $ExitCode = 1;
      $number += $number_of_lines_joined;
      $number_of_lines_joined = 0;
      next MainLoop;
      }

    if ( ! defined $InitCalls { $rr } ) {
      if ( defined $opt_v ) {
        print "+ -" . $rr . "-\n";
        }
      LogError ( $FileName, "Unknown RR found", 1, $number );
      $ExitCode = 1;
      }
    else {
      ( $Error, $Fatal ) = CheckHost ( $host, $domain );
      if ( $Error ne "" ) {
        LogError ( $FileName, $Error, $Fatal, $number );
        $ExitCode = 1 if ( $Fatal > 0 );
        }
      else {
        ( $Error, $Fatal ) = CheckTTL ( $ttl );
        if ( $Error ne "" ) {
          LogError ( $FileName, $Error, $Fatal, $number );
          $ExitCode = 1 if ( $Fatal > 0 );
          }
        else {
          if ( $host ne "@" ) {
            if ( $host =~ /\.$/o ) {
              $fqdn = uc ( $host );
              }
            else {
              $fqdn = uc ( $host . "." . $domain );
              }
            $_ = $fqdn;
            /(\S+?)\.(\S+)/o;
            $prefix = $1;
            $suffix = $2;
            }
          else {
            $fqdn = uc ( $domain );
            $_ = $fqdn;
            /(\S+?)\.(\S+)/o;
            $prefix = $1;
            $suffix = $2;
            }

          if ( defined $Lines { $fqdn } ) {
            $Lines { $fqdn } = $Lines { $fqdn } . " " . $number;
            }
          else {
            $Lines { $fqdn } = $number;
            }

          if ( defined $Records { $fqdn } ) {
            $Records { $fqdn } .= " " . $rr;
            }
          else {
            $Records { $fqdn } = $rr;
            }

          $_ = $InitCalls { $rr };

          ( $Error, $Fatal ) = & $_ ( $GlobalTTL, $host, $class, $ttl, $rr, $rest );

          if ( $Error ne "" ) {
            LogError ( $FileName, $Error, $Fatal, $number );
            $ExitCode = 1 if ( $Fatal > 0 );
            }

          if ( ( $host ne "@" ) && ( $host ne $GlobalDomain ) ) {
            if ( ( $rr eq "NS" ) || ( $rr eq "SOA" ) ) {
              $DomainList { $fqdn } = $fqdn;
              }

            $domain1 = "." . $GlobalDomain;

            if ( ! ( defined $DomainList { $fqdn } ) ) {
              if ( length ( $domain1 ) < length ( $fqdn ) ) {
                $domain2 = substr ( $fqdn, ( length ( $fqdn ) - length ( $domain1 ) ) );
                if ( $domain2 ne $domain1 ) {
                  $DomainList { $fqdn } = $fqdn;
                  }
                }
              }

            foreach ( SplitDomain ( $fqdn, $GlobalDomain ) ) {
              if ( defined $Fqdn { $_ } ) {
                $Fqdn { $_ } .= " " . $number;
                }
              else {
                $Fqdn { $_ } = $number;
                }
              }

            $Fqdn_host { $number } = $host;
            $Fqdn_domain { $number } = $domain;
            $Fqdn_rr { $number } = $rr;
            }
          }
        }
      }
    }
  $number += $number_of_lines_joined;
  $number_of_lines_joined = 0;
  }

#
# The following tie's can now be removed, they are no longer required
# and the memory and disk space can probebly be used better elsewhere

untie %CacheCheckRefHostError;
untie %CacheCheckRefHostFatal;

untie %CacheCheckHostError;
untie %CacheCheckHostFatal;

$Error = "";
$Fatal = 0;

#
# The first big NONO... Not having a SOA record is FATAL

if ( ! ( $Records { $GlobalDomain } =~ /SOA/ ) ) {
  LogError ( $FileName, "No SOA found for domain " . $GlobalDomain, 1, 1);
  $ExitCode = 1;
  }

#
# Because we kept track of the various RR's found for the hosts
# we can now run through them :)

foreach $fqdn ( keys %Records ) {
  undef %rr_list;
  undef %allowed_rr;
  foreach $rr ( split ( /\s+/, $Records { $fqdn } ) ) {
#    if ( ( $rr ne "SIG" ) && ( $rr ne "KEY" ) && ( $rr ne "NXT" ) ) {
      if ( defined $rr_list { $rr } ) {
        $rr_list { $rr } ++;
        }
      else {
        $rr_list { $rr } = 1;
        }
#      }
    }

  foreach $rr ( keys %rr_list ) {
    $allowed_rr { $rr } = $rr_list { $rr };
    }

#
# SIG, KEY, and NXT may exist with _any_ other RR, so to avoid confusion
# we pretend to have never seen them. Any checks for these records should 
# have been done before this point.

  if ( defined $allowed_rr { "SIG" } ) {
    delete $allowed_rr { "SIG" };
    }
  if ( defined $allowed_rr { "KEY" } ) {
    delete $allowed_rr { "KEY" };
    }
  if ( defined $allowed_rr { "NXT" } ) {
    delete $allowed_rr { "NXT" };
    }

#
# Check number of NS records

  if ( defined $rr_list { "NS" } ) {
    if ( $rr_list { "NS" } == 1 ) {
      $Error = "Only one NS record found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    if ( defined $rr_list { "SOA" } ) {

#
# If a SOA RR also exists, then we also allow the following RR's

#
# Nothing to check, all other checks done elsewhere

#
# If ever a RR is defined that is not legal with a SOA RR then
# this is where the check should go

      }
    else {

#
# No SOA RR exists, so no extra RR's allowed

      if ( scalar keys %allowed_rr > 1 ) { 
        if ( defined $opt_v ) {
          print "NOSOA -";
          print scalar keys %allowed_rr;
          print "-\n";
          foreach $line ( keys %allowed_rr ) {
            print "NOSOA +" . $line . "+" . $allowed_rr { $line } . "+\n";
            }
          }
        $Error = "NS record, and other RR's found for \"" . lc $fqdn . "\"";
        $Fatal = 1;
        @_ = split /\s+/,  $Lines { $fqdn };
        LogErrors ( $FileName, $Error, $Fatal, @_ );
        $ExitCode = 1 if ( $Fatal > 0 );
        $Error = "";
        $Fatal = 0;
        }
      }

    }

#
# End of NS RR check

#
# LOC RR check

  if ( defined $rr_list { "LOC" } ) {

    if ( $rr_list { "LOC" } > 1 ) {
      $Error = "Multiple LOC RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of LOC RR check

#
# MX RR check

  if ( defined $rr_list { "MX" } ) {

    if ( $rr_list { "MX" } < 2 ) {
      $Error = "Only 1 MX RR found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of MX RR check

#
# CNAME RR check

  if ( defined $rr_list { "CNAME" } ) {

    if ( scalar keys %allowed_rr > 1 ) {
      $Error = "CNAME record, and other RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 1;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    if ( $rr_list { "CNAME" } > 1 ) {
      $Error = "Multiple CNAME RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 1;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of CNAME RR check

#
# PTR RR check

  if ( defined $rr_list { "PTR" } ) {

    if ( $rr_list { "PTR" } > 1 ) {
      $Error = "Multiple PTR RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of PTR RR check

#
# Check for "*" records.

  if ( $fqdn =~ /\*/o ) {
    foreach ( keys %rr_list ) {
      if ( $_ eq "A" ) {
        $Error = "The use of * A records is actively discouraged";
        $Fatal = 0;
        }
      }
    if ( $Error ne "" ) { 
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }
    }


#
# End of "*" record check

  }

untie %Lines;
untie %Records;

foreach $domain ( keys %DomainList ) {
  if ( defined $Fqdn { $domain } ) {
    foreach $number ( split ( /\s+/, $Fqdn { $domain } ) ) {
      $fqdn = $Fqdn_host { $number } . "." . $Fqdn_domain { $number };
      if ( $Fqdn_rr { $number } eq "A" ) {
        $Error = "glue found for host \"" . $Fqdn_host { $number } . "\" in domain \"" . $Fqdn_domain { $number } . "\"";
        $Fatal = 0;
        LogError ( $FileName, $Error, $Fatal, $number );
        }
      elsif ( $Fqdn_rr { $number } eq "AAAA" ) {
        $Error = "glue found for host \"" . $Fqdn_host { $number } . "\" in domain \"" . $Fqdn_domain { $number } . "\"";
        $Fatal = 0;
        LogError ( $FileName, $Error, $Fatal, $number );
        }
      elsif ( $Fqdn_rr { $number } eq "SOA" ) {
        }
      elsif ( $Fqdn_rr { $number } eq "NS" ) {
        }
      else {
        $Error = "non-glue record for host \"" . $Fqdn_host { $number } . "\" below bottom of zone in domain \"" . $Fqdn_domain { $number } . "\"";
        $Fatal = 1;
        LogError ( $FileName, $Error, $Fatal, $number );
        $ExitCode = 1;
        }
      }
    }
  }

if ( defined $opt_s ) {
  print $GlobalSerial . "\n";
  }

exit $ExitCode;
