#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_SOA {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  $_ = $list [ 2 ];
  s/^0+//;
  if ( $GlobalSerial eq -1 ) {
    $GlobalSerial = $_;
    }

  if ( defined $opt_S ) {
    print $GlobalSerial . "\n";
    exit;
    }

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 7 ) {
    $Error = "Invalid number of options in SOA";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 0 ], "primary server" );
    if ( $Error eq "" ) {
      ( $Error, $Fatal ) = CheckRefHost ( $list [ 1 ], "contact information" );
      if ( $Error eq "" ) {
        if ( ! ( $list [ 2 ] =~ /^\d+$/ ) ) {
          $Error = "Serial is not numeric in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 3 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Refresh is not valid in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 4 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Retry is not valid in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 5 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Expire is not valid in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 6 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Minimum TTL is not valid in SOA";
          $Fatal = 1;
          }
#        else {
#          if ( $list [ 3 ] != 28800 ) {
#            $Error = "Reresh is not the recomended 28800 (8 hours)";
#            }
#          if ( $list [ 4 ] != 7200 ) {
#            $Error = "Retry is not the recomended 7200 (2 hours)";
#            }
#          if ( $list [ 5 ] != 604800 ) {
#            $Error = "Expire is not the recomended 604800 (7 days)";
#            }
#          if ( $list [ 6 ] != 86400 ) {
#            $Error = "Minimum is not the recomended 86400 (1 day)";
#            }
#          }
        else {
          $list [ 3 ] = ConvertAge ( uc $list [ 3 ] ) ;
          $list [ 4 ] = ConvertAge ( uc $list [ 4 ] );
          $list [ 5 ] = ConvertAge ( uc $list [ 5 ] );
          $list [ 6 ] = ConvertAge ( uc $list [ 6 ] );
          if ( $list [ 3 ] < $SOA_Refresh_Min ) {
            $Error = "Refresh is smaller then the recomended " . $SOA_Refresh_Min;
            }
          if ( $list [ 3 ] > $SOA_Refresh_Max ) {
            $Error = "Refresh is larger then the recomended " . $SOA_Refresh_Max;
            }
          elsif ( $list [ 4 ] < $SOA_Retry_Min ) {
            $Error = "Retry is smaller then the recomended " . $SOA_Retry_Min;
            }
          elsif ( $list [ 4 ] > $SOA_Retry_Max ) {
            $Error = "Retry is larger then the recomended " . $SOA_Retry_Max;
            }
          elsif ( $list [ 5 ] < $SOA_Expire_Min ) {
            $Error = "Expire is smaller then the recomended " . $SOA_Expire_Min;
            }
          elsif ( $list [ 5 ] > $SOA_Expire_Max ) {
            $Error = "Expire is larger then the recomended " . $SOA_Expire_Max;
            }
          elsif ( $TTLbyDirective == -1 ) {
            if ( $list [ 6 ] < $SOA_Minimum_Min ) {
              $Error = "Minimum is smaller then the recomended " . $SOA_Minimum_Min;
              }
            elsif ( $list [ 6 ] > $SOA_Minimum_Max ) {
              $Error = "Minimum is greater then the recomended " . $SOA_Minimum_Min;
              }
            }
          elsif ( $TTLbyDirective != -1 ) {
            if ( $list [ 6 ] < $SOA_Minimum_Negative_Min ) {
              $Error = "Minimum is smaller then the recomended " . $SOA_Minimum_Negative_Min;
              }
            elsif ( $list [ 6 ] > $SOA_Minimum_Negative_Max ) {
              $Error = "Minimum is greater then the recomended " . $SOA_Minimum_Negative_Max;
              }
            }
          }
        }
      }
    }

  return $Error, $Fatal;
  }

$InitCalls { "SOA" } = \ & check_SOA;
