#!/bin/sh

ZONEFILE=$1

exec > /dev/null 2>&1

./zonecheck -d test.zone ${ZONEFILE}

exit $?
