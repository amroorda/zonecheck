# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The weird and wonderfull world of DNS combined with perl

### How do I get set up? ###

Read and understand the source, although it is written in perl, so that might be an issue.

There is a usage line when started without any parameters. Be sure to 'make' the complete
script first.

### Contribution guidelines ###

New tests, usage reports, and other weird things. Code welcome, but not required.

### Who do I talk to? ###

Anne Marcel Roorda <marcel@slowthinkers.net>