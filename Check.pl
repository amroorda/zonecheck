my ( %RegExp );

#
# If $ValidChars needs to be modified, then please also modify
# $RegExp { host } { absolute } and $RegExp { host } { relative }
# below, but don't allow the '.' in there.

my ( $ValidChars ) = "[a-zA-Z0-9\._-]";

$RegExp { IPv4 } { full } = '^((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))$';
$RegExp { IPv4 } { short } = '^(((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d)))))|(((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d)))))|(((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d)))))$';
$RegExp { IPv6 } { full } = '^(([\dabcdefABCDEF]{1,4}:){7}[\dabcdefABCDEF]{1,4})$';
$RegExp { IPv6 } { short } = '^((([\dabcdefABCDEF]{1,4}:){1,7}:[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,6}:[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,5}:([\dabcdefABCDEF]{1,4}:){0,1}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,4}:([\dabcdefABCDEF]{1,4}:){0,2}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,3}:([\dabcdefABCDEF]{1,4}:){0,3}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,2}:([\dabcdefABCDEF]{1,4}:){0,4}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,1}:([\dabcdefABCDEF]{1,4}:){0,5}[\dabcdefABCDEF]{1,4})|::([\dabcdefABCDEF]{1,4}:){0,6}:([\dabcdefABCDEF]{1,4}))$';

$RegExp { host } { absolute } = '^((([\*a-zA-Z0-9_-]{1,64}\.)([a-zA-Z0-9_-]{1,64}\.){1,})|(([a-zA-Z0-9_-]{1,64}\.){1,})|([\*a-zA-Z0-9_-]{1,64}\.))$';

$RegExp { host } { relative } = '^(([\*a-zA-Z0-9_-]{1,64})|(([\*a-zA-Z0-9_-]{1,64}\.){1,}([a-zA-Z0-9_-]{1,64})))$';

$RegExp { age } { numeric } = '\d+';

#$RegExp { age } { alphanumeric } = '(\d+S){0,1}(\d+M){0,1}(\d+H){0,1}(\d+D){0,1}(\d+W){0,1}';
$RegExp { age } { alphanumeric } = '(\d+[SMHDW])+';

sub ConvertAge {
  my ( $Age ) = shift @_;

  if ( $Age =~ /^$RegExp{age}{numeric}$/ ) {
    return $Age;
    }
  elsif ( $Age =~ /^$RegExp{age}{alphanumeric}$/ ) {
    $_ = $Age;
    $Age = 0;
    s/(\d+)/ $1/g;
    @_ = split;
    foreach ( @_ ) {
      if ( $_ =~ /S/ ) {
        /(\d+)/;
        $Age += $1;
        }
      elsif ( $_ =~ /M/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 );
        }
      elsif ( $_ =~ /H/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 * 60 );
        }
      elsif ( $_ =~ /D/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 * 60 * 24 );
        }
      elsif ( $_ =~ /W/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 * 60 * 24 * 7 );
        }
      }
    return $Age;
    }
  else {
    return undef;
    }
  }

sub CheckRefHost {

  my ( $hostname ) = shift @_;
  my ( $what ) = shift @_;

  my ( @chars );

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  $_ = $hostname;
  s/$ValidChars//go;

  if ( defined $CacheCheckRefHostError { $hostname } ) {
    if ( $CacheCheckRefHostError { $hostname } ne "" ) {
      $Error = $CacheCheckRefHostError { $hostname } . $what;
      $Fatal = $CacheCheckRefHostFatal { $hostname };
      }
    }
  else {
    if ( length ( $_ ) > 0 ) {
      $Error = "Illegal characters found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /\.\./o ) {
      $Error = "Double \".\" in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv4}{full}/o ) {
      $Error = "Hostname required but IPv4 address found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv4}{short}/o ) {
      $Error = "Hostname required but short IPv4 address found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv6}{full}/o ) {
      $Error = "Hostname required but IPv6 address found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv6}{short}/o ) {
      $Error = "Hostname required but short IPv6 address found in ";
      $Fatal = 1;
      }
    elsif ( ! ( $hostname =~ /^.*\.$/o ) ) {
      $Error = "Relative host found in ";
      }
    elsif ( ! ( ( $hostname =~ /$RegExp{host}{relative}/o ) || ( $hostname =~ /$RegExp{host}{absolute}/o ) ) ) {
      $Error = "Invalid referenced hostname found in ";
      $Fatal = 1;
      }
    $CacheCheckRefHostError { $hostname } = $Error;
    $CacheCheckRefHostFatal { $hostname } = $Fatal;
    if ( $Error ne "" ) {
      $Error = $Error . $what;
      }
    }

return $Error, $Fatal;
}

sub CheckHost {
  my ( $hostname ) = uc shift @_;
  my ( $domain ) = uc shift @_;

  my ( @chars );

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  if ( $hostname =~ /@/o ) {
    $hostname = $domain;
    }

  if ( defined $CacheCheckHostError { $hostname } ) {
    if ( $CacheCheckHostError { $hostname } ne "" ) {
      $Error = $CacheCheckHostError { $hostname };
      $Fatal = $CacheCheckHostFatal { $hostname };
      }
    }
  else {
    $_ = $hostname;
    s/$ValidChars//og;
    s/\*//og;
    s/\///og;
  
    if ( length ( $_ ) > 0 ) {
      $Error = "Illegal characters found";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /\.\./o ) {
      $Error = "Double \".\" in first field";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /^\./o ) {
      $Error = "Hostnames cannot start with a \".\"";
      $Fatal = 1;
      }
    elsif ( ! ( ( $hostname =~ /$RegExp{host}{relative}/o ) || ( $hostname =~ /$RegExp{host}{absolute}/o ) ) ) {
      $Error = "Invalid hostname";
      $Fatal = 1;
      }
    $CacheCheckHostError { $hostname } = $Error;
    $CacheCheckHostFatal { $hostname } = $Fatal;
    }
  return $Error, $Fatal;
  }

sub CheckTTL {
  my ( $ttl ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  if ( ( $ttl ne "" ) && ( $ttl < 10 ) ) {
    $Error = "TTL to low";
    }

  return $Error, $Fatal;
  }

sub NormalizeInput {
  my ( $InputLine ) = shift @_;

  $_ = $InputLine;

  if ( $_ =~ /\"/o ) {
    s/^\s+(.+\".*)$/ $1/o;
    s/^(.+?)\s+(.*)$/$1 $2/o;
    s/^(.+ .+?)\s+(.*)$/$1 $2/o;
    s/^(.+ .+ .+?)\s+(.*)$/$1 $2/o;
    s/^(.+ .+ .+ .+?)\s+(.*)$/$1 $2/o;
    }

  else {
    s/\s+/ /og;
    }
  s/^\$INCLUDE.*//o;
  s/^\$GENERATE.*//o;
  s/;.*$//o;
  s/\s+$//og;

  return uc $_;
  }

sub NormalizeInput2 {
  my ( $InputLine ) = shift @_;

  my ( $one, $two );
  my ( @records );
  my ( $element );

  $_ = $InputLine;

  @_ = ();

  if ( $_ =~ /\"/o ) {
    /(.*?)(\".*)/o;
    $one = $1;
    $two = $2;
    @records = split /\s+/, $one;
    push @records, $two;
    }
  else {
    @records = split /\s+/, $_;
    }

  $element = shift @records;

  push @_,  $element;

  $element = shift @records;

  if ( $element =~ /^$RegExp{age}{numeric}|$RegExp{age}{alphanumeric}$/o ) {
    push @_, $element;
    $element = shift @records;
    }

  if ( ! defined $classes { $element } ) {
    push @_, "IN";
    }
  else {
    push @_, $element;
    $element = shift @records;
    }

  push @_, $element;

  foreach ( @records ) {
    push @_, $_;
    }

  $_ = join " ", @_;

  return uc $_;
  }

sub LogError {
  my ( $FileName ) = shift @_;
  my ( $Error ) = shift @_;
  my ( $Fatal ) = shift @_;
  my ( $number ) = shift @_;

  if ( $Fatal > 0 ) {
    print STDERR $FileName . ": line " . $number . ": syntax error: " . $Error . "\n";
    print STDERR $InputLine { $number } . "\n";
    }
  elsif ( ! ( defined $opt_s ) ) {
    print STDERR $FileName . ": line " . $number . ": warning: " . $Error . "\n";
    print STDERR $InputLine { $number } . "\n";
    }
}

sub LogErrors {
  my ( $FileName ) = shift @_;
  my ( $Error ) = shift @_;
  my ( $Fatal ) = shift @_;
  my ( @number ) = @_;

  my ( $ErrorNumbers ) = join ',', @number;

  if ( $Fatal > 0 ) {
    print STDERR $FileName . ": line " . $ErrorNumbers . ": syntax error: " . $Error . "\n";
    foreach $ErrorNumbers ( @number ) {
      print STDERR $InputLine { $ErrorNumbers } . "\n";
      }
    }
  elsif ( ! ( defined $opt_s ) ) {
    print STDERR $FileName . ": line " . $ErrorNumbers . ": warning: " . $Error . "\n";
    foreach $ErrorNumbers ( @number ) {
      print STDERR $InputLine { $ErrorNumbers } . "\n";
      }
    }
}

sub SplitDomain {
  my ( $Fqdn ) = shift @_;
  my ( $Domain ) = shift @_;

  my ( $Domain1 );
  my ( $prefix );

  my ( @Result );

  if ( ( $Fqdn eq $Domain ) | ( length ( $Domain ) >= length ( $Fqdn ) ) ) {
    push @Result, $Fqdn;
    }
  else {
    $Domain = "." . $Domain;
    $Domain1 = substr ( $Fqdn, ( length ( $Fqdn ) - length ( $Domain ) ) );
    if ( $Domain1 eq $Domain ) {
      $prefix = substr ( $Fqdn, 0, ( length ( $Fqdn ) - length ( $Domain ) ) ); 
      push @Result, ( $prefix . $Domain );
      while ( $prefix =~ /\./o ) {
        $_ = $prefix;
        /\S+?\.(\S+)/o;
        $prefix = $1;
        push @Result, ( $prefix . $Domain );
        }
      }
    else {
      push @Result, $Fqdn;
      }
    }

  return @Result;
}

