#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

#
# We need to know where the script has been installed
# so unshift that location onto @INC

#use strict;

use DB_File;
#use NDBM_File;

# my ( $opt_d, $opt_s, $opt_S, $opt_v, $opt_V );

use Getopt::Std;
use File::Basename;

use vars qw ( $opt_c, $opt_d, $opt_h, $opt_m, $opt_s, $opt_S, $opt_v, $opt_V );

#
# %Input will contain the actual zone file

my ( %InputLine );
my ( $Input );

#
# %DomainList will contain a list of all domains seen

my ( %DomainList );

#
# $number and $number2 will be used as index to both
# Input, and Buffer

my ( $number ) = 0;
my ( $number2 ) = 0;
my ( $number_of_lines_joined ) = 0;

#
# use vars qw ( $opt_d, $opt_s, $opt_S, $optv );
# $i will be used as a temporary numeric variable in
# some loops

my ( $i );

#
# $line will be used as a temporary string variable
# in some loops

my ( $line );

#
# $domain will hold the domain name that the current
# zonefile contains

my ( $domain ) = "";
my ( $domain1 );
my ( $domain2 );

#
# $host will contain the hostname for forward zones
# or the ip number for reversed zones

my ( $host );

#
# @records is used to store the diffent parts of a complete line
# after it has been split

my ( @records );

#
# $element is used to temporarily hold an entry from
# @records

my ( $element );

#
# $class, $ttl, $rr, $rest are used to hold the different
# parts of each record in the main loop

my ( $class, $ttl, $rr, $rest );

#
# $GlobalTTL for now contains the TTL as found in the SOA
# record

my ( $GlobalTTL );

#
# $TTLbyDirective contains the last vallue given by a $TTL
# directive, or -1 if none has been found yet

my ( $TTLbyDirective ) = -1;

#
# $GlobalSerial contains the Serial of the zone as found in the SOA
# record

my ( $GlobalSerial ) = -1;

#
# $GoobalDomain will contain the domain name we are currently checking

my ( $GlobalDomain );

#
# %Records is a hash of a hash containing
# host names, and the RR's found for them

my ( %Records );

#
# %Fqdn is a hash of a hash used to hold hostname information
# hashed on FQDN and RR

my ( %Fqdn );
my ( %Fqdn_prefix );
my ( %Fqdn_suffix );
my ( %Fqdn_host );
my ( %Fqdn_domain );
my ( %Fqdn_rr );

#
#

my ( $SOA_Refresh_Min ) = 28800;
my ( $SOA_Refresh_Max ) = 28800;
my ( $SOA_Retry_Min ) = 7200;
my ( $SOA_Retry_Max ) = 7200;
my ( $SOA_Expire_Min ) = 604800;
my ( $SOA_Expire_Max ) = 604800;
my ( $SOA_Minimum_Min ) = 86400;
my ( $SOA_Minimum_Max ) = 86400;
my ( $SOA_Minimum_Negative_Min ) = 10800;
my ( $SOA_Minimum_Negative_Max ) = 10800;

#
# $fqdn is a temp var. for holding one of the keys of %Fqdn

my ( $fqdn );
my ( $prefix );
my ( $suffix );

#
# %Domain and %Host are used to hold a has of a hash
# containing the Domain and Host as found in the
# zonefile. Hashing takes place on the hostname part only,
# and any sub-domain part are prepended to the domain.

my ( %Domain );
my ( %Host );

#
# %Lines contains a list of linenumbers hashed on
# hostname

my ( %Lines );

#
# A general purpose var. used all of one times.

my ( $Line );

#
# %Buffer is used to store the version
# of the zonefile that is worked on
#
# Hashing is done on linenumber same as for
# %Input

my ( $Buffer );

#
# %InitCalls holds pointers to the different check routines
# as returned from the modules
#
# Hashing is done on RR name

my ( %InitCalls );

#
# $Error is used to contain the Error message returned to
# the user

my ( $Error ) = "";

#
# $Fatal is used to defferentiate between fatal and
# non fatal errors

my ( $Fatal ) = 0;

#
# $ExitCode holds the final exitcode used when finished
# and will be non zero if fatal errors have been found

my ( $ExitCode ) = 0;

#
# %classes holds all valid classes

my ( %classes ) = ( IN => IN, CS => CS, CH => CH, HS => HS, CHAOS => CHAOS );

#
# -d can be used to set the domain name

getopts ( 'd:c:h:msSvV' );

if ( defined $opt_c ) {
  $DB_BTREE->{'cachesize'} = $opt_c;
  }
else {
  $DB_BTREE->{'cachesize'} = 8000;
  }

if ( defined $opt_h ) {
  @_ = split ( /,/, $opt_h );
  if ( scalar @_ ne 10 ) {
    print STDERR "Error: -h takes 10 numeric options seperted by \',\'\n";
    exit 1;
    }
  foreach ( @_ ) {
    if ( ! ( $_ =~ /^\d+$/ ) ) {
      print STDERR "Error: -h takes 10 numeric options seperted by \',\'\n";
      exit 1;
      }
    }
  $SOA_Refresh_Min  = $_ [ 0 ];
  $SOA_Refresh_Max  = $_ [ 1 ];
  $SOA_Retry_Min   = $_ [ 2 ];
  $SOA_Retry_Max   = $_ [ 3 ];
  $SOA_Expire_Min  = $_ [ 4 ];
  $SOA_Expire_Max  = $_ [ 5 ];
  $SOA_Minimum_Min = $_ [ 6 ];
  $SOA_Minimum_Max = $_ [ 7 ];
  $SOA_Minimum_Negative_Min = $_ [ 8 ];
  $SOA_Minimum_Negative_Max = $_ [ 9 ];
  }

if ( defined $opt_d ) {
  $domain = $opt_d;
  }

if ( defined $opt_V ) {
  print "zonecheck 0.9-7\n";
  exit;
  }

if ( defined $opt_m ) {
  goto HERE if ! open INPUT, "/usr/local/etc/zonecheck.conf";

  my ( %Valid_Options ) = (
    SOA_Refresh_Min => $SOA_Refresh_Min,
    SOA_Refresh_Max => $SOA_Refresh_Max,
    SOA_Retry_Min => $SOA_Retry_Min,
    SOA_Retry_Max => $SOA_Retry_Max,
    SOA_Expire_Min => $SOA_Expire_Min,
    SOA_Expire_Max => $SOA_Expire_Max,
    SOA_Minimum_Min => $SOA_Minimum_Min,
    SOA_Minimum_Max => $SOA_Minimum_Max,
    SOA_Minimum_Negative_Min => $SOA_Minimum_Negative_Min,
    SOA_Minimum_Negative_Max => $SOA_Minimum_Negative_Max
    );

  while ( <INPUT> ) {
    chomp;
    if ( defined $opt_v ) {
      print " + " . $_ . "\n";
      }
    if ( $_ =~ /^\S+\s+\d+$/ ) {
      /^(\S+)\s+(\d+)$/;
      if ( defined $Valid_Options { $1 } ) {
        $Valid_Options { $1 } = $2;
        }
      }
    }

  $SOA_Refresh_Min = $Valid_Options { "SOA_Refresh_Min" };
  $SOA_Refresh_Max = $Valid_Options { "SOA_Refresh_Max" };
  $SOA_Retry_Min = $Valid_Options { "SOA_Retry_Min" };
  $SOA_Retry_Max = $Valid_Options { "SOA_Retry_Max" };
  $SOA_Expire_Min = $Valid_Options { "SOA_Expire_Min" };
  $SOA_Expire_Max = $Valid_Options { "SOA_Expire_Max" };
  $SOA_Minimum_Min = $Valid_Options { "SOA_Minimum_Min" };
  $SOA_Minimum_Negative_Min = $Valid_Options { "SOA_Minimum_Negative_Min" };
  $SOA_Minimum_Negative_Max = $Valid_Options { "SOA_Minimum_Negative_Max" };

  close INPUT;

  HERE:
  }

tie %InputLine, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %InputLine\n";

my ( %CacheCheckRefHostError );
my ( %CacheCheckRefHostFatal );

tie %CacheCheckRefHostError, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckRefHostError\n";
tie %CacheCheckRefHostFatal, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckRefHostFatal\n";

tie %Lines, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Lines\n";
tie %Records, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Records\n";

my ( %CacheCheckHostError );
my ( %CacheCheckHostFatal );

tie %CacheCheckHostError, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckHostError\n";
tie %CacheCheckHostFatal, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckHostFatal\n";

tie %DomainList, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %DomainList\n";
tie %Fqdn, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn\n";
tie %Fqdn_rr, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn_rr\n";
tie %Fqdn_domain, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn_domain\n";
tie %Fqdn_host, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn_host\n";

my ( %rr_list );
my ( %allowed_rr );
my ( %allowed_rr_2 );

$DB_BTREE->{'cachesize'} = 4000;

tie %rr_list, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %rr_list\n";
