#
#
#

SRCFILES=disclaimer.pl header.pl Check.pl \
	A.pl.module A6.pl.module AAAA.pl.module CERT.pl.module \
	CNAME.pl.module HINFO.pl.module KEY.pl.module LOC.pl.module \
	MX.pl.module NS.pl.module NSAP.pl.module NXT.pl.module \
	PTR.pl.module RP.pl.module SIG.pl.module SOA.pl.module \
	SRV.pl.module TXT.pl.module WKS.pl.module \
	footer.pl

PERLCC=/usr/local/bin/perlcc
PERLCCOPTS=

all:	zonecheck

zonecheck.pl:	$(SRCFILES)
	@cat $(SRCFILES) > zonecheck.pl

zonecheck:	zonecheck.pl
	@cp zonecheck.pl zonecheck

clean:
	@rm -f zonecheck.log
	@rm -f zonecheck.pl
	@rm -f zonecheck.c
	@rm -f zonecheck.non-strip
	@rm -f zonecheck

distclean: clean
