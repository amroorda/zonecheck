#!/usr/local/bin/perl

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

my ( @ Disclaimer ) = ( "Copyright 2001 Anne Marcel Roorda. All rights reserved.",
  "",
  "Redistribution and use in source and binary forms, with or without",
  "modification, are permitted provided that the following conditions",
  "are met:",
  "",
  "    1.Redistributions of source code must retain the above copyright",
  "      notice, this list of conditions and the following disclaimer.",
  "    2.Redistributions in binary form must reproduce the above",
  "      copyright notice, this list of conditions and the following",
  "      disclaimer in the documentation and/or other materials",
  "      provided with the distribution.",
  "",
  "THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR",
  "IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED",
  "WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE",
  "ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE",
  "LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR",
  "CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF",
  "SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR",
  "BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF",
  "LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING",
  "NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS",
  "SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." );
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

#
# We need to know where the script has been installed
# so unshift that location onto @INC

#use strict;

use DB_File;
#use NDBM_File;

# my ( $opt_d, $opt_s, $opt_S, $opt_v, $opt_V );

use Getopt::Std;
use File::Basename;

use vars qw ( $opt_c, $opt_d, $opt_h, $opt_m, $opt_s, $opt_S, $opt_v, $opt_V );

#
# %Input will contain the actual zone file

my ( %InputLine );
my ( $Input );

#
# %DomainList will contain a list of all domains seen

my ( %DomainList );

#
# $number and $number2 will be used as index to both
# Input, and Buffer

my ( $number ) = 0;
my ( $number2 ) = 0;
my ( $number_of_lines_joined ) = 0;

#
# use vars qw ( $opt_d, $opt_s, $opt_S, $optv );
# $i will be used as a temporary numeric variable in
# some loops

my ( $i );

#
# $line will be used as a temporary string variable
# in some loops

my ( $line );

#
# $domain will hold the domain name that the current
# zonefile contains

my ( $domain ) = "";
my ( $domain1 );
my ( $domain2 );

#
# $host will contain the hostname for forward zones
# or the ip number for reversed zones

my ( $host );

#
# @records is used to store the diffent parts of a complete line
# after it has been split

my ( @records );

#
# $element is used to temporarily hold an entry from
# @records

my ( $element );

#
# $class, $ttl, $rr, $rest are used to hold the different
# parts of each record in the main loop

my ( $class, $ttl, $rr, $rest );

#
# $GlobalTTL for now contains the TTL as found in the SOA
# record

my ( $GlobalTTL );

#
# $TTLbyDirective contains the last vallue given by a $TTL
# directive, or -1 if none has been found yet

my ( $TTLbyDirective ) = -1;

#
# $GlobalSerial contains the Serial of the zone as found in the SOA
# record

my ( $GlobalSerial ) = -1;

#
# $GoobalDomain will contain the domain name we are currently checking

my ( $GlobalDomain );

#
# %Records is a hash of a hash containing
# host names, and the RR's found for them

my ( %Records );

#
# %Fqdn is a hash of a hash used to hold hostname information
# hashed on FQDN and RR

my ( %Fqdn );
my ( %Fqdn_prefix );
my ( %Fqdn_suffix );
my ( %Fqdn_host );
my ( %Fqdn_domain );
my ( %Fqdn_rr );

#
#

my ( $SOA_Refresh_Min ) = 28800;
my ( $SOA_Refresh_Max ) = 28800;
my ( $SOA_Retry_Min ) = 7200;
my ( $SOA_Retry_Max ) = 7200;
my ( $SOA_Expire_Min ) = 604800;
my ( $SOA_Expire_Max ) = 604800;
my ( $SOA_Minimum_Min ) = 86400;
my ( $SOA_Minimum_Max ) = 86400;
my ( $SOA_Minimum_Negative_Min ) = 10800;
my ( $SOA_Minimum_Negative_Max ) = 10800;

#
# $fqdn is a temp var. for holding one of the keys of %Fqdn

my ( $fqdn );
my ( $prefix );
my ( $suffix );

#
# %Domain and %Host are used to hold a has of a hash
# containing the Domain and Host as found in the
# zonefile. Hashing takes place on the hostname part only,
# and any sub-domain part are prepended to the domain.

my ( %Domain );
my ( %Host );

#
# %Lines contains a list of linenumbers hashed on
# hostname

my ( %Lines );

#
# A general purpose var. used all of one times.

my ( $Line );

#
# %Buffer is used to store the version
# of the zonefile that is worked on
#
# Hashing is done on linenumber same as for
# %Input

my ( $Buffer );

#
# %InitCalls holds pointers to the different check routines
# as returned from the modules
#
# Hashing is done on RR name

my ( %InitCalls );

#
# $Error is used to contain the Error message returned to
# the user

my ( $Error ) = "";

#
# $Fatal is used to defferentiate between fatal and
# non fatal errors

my ( $Fatal ) = 0;

#
# $ExitCode holds the final exitcode used when finished
# and will be non zero if fatal errors have been found

my ( $ExitCode ) = 0;

#
# %classes holds all valid classes

my ( %classes ) = ( IN => IN, CS => CS, CH => CH, HS => HS, CHAOS => CHAOS );

#
# -d can be used to set the domain name

getopts ( 'd:c:h:msSvV' );

if ( defined $opt_c ) {
  $DB_BTREE->{'cachesize'} = $opt_c;
  }
else {
  $DB_BTREE->{'cachesize'} = 8000;
  }

if ( defined $opt_h ) {
  @_ = split ( /,/, $opt_h );
  if ( scalar @_ ne 10 ) {
    print STDERR "Error: -h takes 10 numeric options seperted by \',\'\n";
    exit 1;
    }
  foreach ( @_ ) {
    if ( ! ( $_ =~ /^\d+$/ ) ) {
      print STDERR "Error: -h takes 10 numeric options seperted by \',\'\n";
      exit 1;
      }
    }
  $SOA_Refresh_Min  = $_ [ 0 ];
  $SOA_Refresh_Max  = $_ [ 1 ];
  $SOA_Retry_Min   = $_ [ 2 ];
  $SOA_Retry_Max   = $_ [ 3 ];
  $SOA_Expire_Min  = $_ [ 4 ];
  $SOA_Expire_Max  = $_ [ 5 ];
  $SOA_Minimum_Min = $_ [ 6 ];
  $SOA_Minimum_Max = $_ [ 7 ];
  $SOA_Minimum_Negative_Min = $_ [ 8 ];
  $SOA_Minimum_Negative_Max = $_ [ 9 ];
  }

if ( defined $opt_d ) {
  $domain = $opt_d;
  }

if ( defined $opt_V ) {
  print "zonecheck 0.9-7\n";
  exit;
  }

if ( defined $opt_m ) {
  goto HERE if ! open INPUT, "/usr/local/etc/zonecheck.conf";

  my ( %Valid_Options ) = (
    SOA_Refresh_Min => $SOA_Refresh_Min,
    SOA_Refresh_Max => $SOA_Refresh_Max,
    SOA_Retry_Min => $SOA_Retry_Min,
    SOA_Retry_Max => $SOA_Retry_Max,
    SOA_Expire_Min => $SOA_Expire_Min,
    SOA_Expire_Max => $SOA_Expire_Max,
    SOA_Minimum_Min => $SOA_Minimum_Min,
    SOA_Minimum_Max => $SOA_Minimum_Max,
    SOA_Minimum_Negative_Min => $SOA_Minimum_Negative_Min,
    SOA_Minimum_Negative_Max => $SOA_Minimum_Negative_Max
    );

  while ( <INPUT> ) {
    chomp;
    if ( defined $opt_v ) {
      print " + " . $_ . "\n";
      }
    if ( $_ =~ /^\S+\s+\d+$/ ) {
      /^(\S+)\s+(\d+)$/;
      if ( defined $Valid_Options { $1 } ) {
        $Valid_Options { $1 } = $2;
        }
      }
    }

  $SOA_Refresh_Min = $Valid_Options { "SOA_Refresh_Min" };
  $SOA_Refresh_Max = $Valid_Options { "SOA_Refresh_Max" };
  $SOA_Retry_Min = $Valid_Options { "SOA_Retry_Min" };
  $SOA_Retry_Max = $Valid_Options { "SOA_Retry_Max" };
  $SOA_Expire_Min = $Valid_Options { "SOA_Expire_Min" };
  $SOA_Expire_Max = $Valid_Options { "SOA_Expire_Max" };
  $SOA_Minimum_Min = $Valid_Options { "SOA_Minimum_Min" };
  $SOA_Minimum_Negative_Min = $Valid_Options { "SOA_Minimum_Negative_Min" };
  $SOA_Minimum_Negative_Max = $Valid_Options { "SOA_Minimum_Negative_Max" };

  close INPUT;

  HERE:
  }

tie %InputLine, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %InputLine\n";

my ( %CacheCheckRefHostError );
my ( %CacheCheckRefHostFatal );

tie %CacheCheckRefHostError, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckRefHostError\n";
tie %CacheCheckRefHostFatal, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckRefHostFatal\n";

tie %Lines, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Lines\n";
tie %Records, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Records\n";

my ( %CacheCheckHostError );
my ( %CacheCheckHostFatal );

tie %CacheCheckHostError, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckHostError\n";
tie %CacheCheckHostFatal, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %CacheCheckHostFatal\n";

tie %DomainList, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %DomainList\n";
tie %Fqdn, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn\n";
tie %Fqdn_rr, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn_rr\n";
tie %Fqdn_domain, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn_domain\n";
tie %Fqdn_host, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %Fqdn_host\n";

my ( %rr_list );
my ( %allowed_rr );
my ( %allowed_rr_2 );

$DB_BTREE->{'cachesize'} = 4000;

tie %rr_list, 'DB_File', undef, O_RDWR|O_CREAT, 0640, $DB_BTREE or die "Unable to open DB_File for %rr_list\n";
my ( %RegExp );

#
# If $ValidChars needs to be modified, then please also modify
# $RegExp { host } { absolute } and $RegExp { host } { relative }
# below, but don't allow the '.' in there.

my ( $ValidChars ) = "[a-zA-Z0-9\._-]";

$RegExp { IPv4 } { full } = '^((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))$';
$RegExp { IPv4 } { short } = '^(((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d)))))|(((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d)))))|(((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d))))\.\.((\d)|(\d\d)|([01]\d\d)|(2((5[012345])|([01234]\d)))))$';
$RegExp { IPv6 } { full } = '^(([\dabcdefABCDEF]{1,4}:){7}[\dabcdefABCDEF]{1,4})$';
$RegExp { IPv6 } { short } = '^((([\dabcdefABCDEF]{1,4}:){1,7}:[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,6}:[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,5}:([\dabcdefABCDEF]{1,4}:){0,1}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,4}:([\dabcdefABCDEF]{1,4}:){0,2}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,3}:([\dabcdefABCDEF]{1,4}:){0,3}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,2}:([\dabcdefABCDEF]{1,4}:){0,4}[\dabcdefABCDEF]{1,4})|(([\dabcdefABCDEF]{1,4}:){0,1}:([\dabcdefABCDEF]{1,4}:){0,5}[\dabcdefABCDEF]{1,4})|::([\dabcdefABCDEF]{1,4}:){0,6}:([\dabcdefABCDEF]{1,4}))$';

$RegExp { host } { absolute } = '^((([\*a-zA-Z0-9_-]{1,64}\.)([a-zA-Z0-9_-]{1,64}\.){1,})|(([a-zA-Z0-9_-]{1,64}\.){1,})|([\*a-zA-Z0-9_-]{1,64}\.))$';

$RegExp { host } { relative } = '^(([\*a-zA-Z0-9_-]{1,64})|(([\*a-zA-Z0-9_-]{1,64}\.){1,}([a-zA-Z0-9_-]{1,64})))$';

$RegExp { age } { numeric } = '\d+';

#$RegExp { age } { alphanumeric } = '(\d+S){0,1}(\d+M){0,1}(\d+H){0,1}(\d+D){0,1}(\d+W){0,1}';
$RegExp { age } { alphanumeric } = '(\d+[SMHDW])+';

sub ConvertAge {
  my ( $Age ) = shift @_;

  if ( $Age =~ /^$RegExp{age}{numeric}$/ ) {
    return $Age;
    }
  elsif ( $Age =~ /^$RegExp{age}{alphanumeric}$/ ) {
    $_ = $Age;
    $Age = 0;
    s/(\d+)/ $1/g;
    @_ = split;
    foreach ( @_ ) {
      if ( $_ =~ /S/ ) {
        /(\d+)/;
        $Age += $1;
        }
      elsif ( $_ =~ /M/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 );
        }
      elsif ( $_ =~ /H/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 * 60 );
        }
      elsif ( $_ =~ /D/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 * 60 * 24 );
        }
      elsif ( $_ =~ /W/ ) {
        /(\d+)/;
        $Age += ( $1 * 60 * 60 * 24 * 7 );
        }
      }
    return $Age;
    }
  else {
    return undef;
    }
  }

sub CheckRefHost {

  my ( $hostname ) = shift @_;
  my ( $what ) = shift @_;

  my ( @chars );

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  $_ = $hostname;
  s/$ValidChars//go;

  if ( defined $CacheCheckRefHostError { $hostname } ) {
    if ( $CacheCheckRefHostError { $hostname } ne "" ) {
      $Error = $CacheCheckRefHostError { $hostname } . $what;
      $Fatal = $CacheCheckRefHostFatal { $hostname };
      }
    }
  else {
    if ( length ( $_ ) > 0 ) {
      $Error = "Illegal characters found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /\.\./o ) {
      $Error = "Double \".\" in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv4}{full}/o ) {
      $Error = "Hostname required but IPv4 address found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv4}{short}/o ) {
      $Error = "Hostname required but short IPv4 address found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv6}{full}/o ) {
      $Error = "Hostname required but IPv6 address found in ";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /$RegExp{IPv6}{short}/o ) {
      $Error = "Hostname required but short IPv6 address found in ";
      $Fatal = 1;
      }
    elsif ( ! ( $hostname =~ /^.*\.$/o ) ) {
      $Error = "Relative host found in ";
      }
    elsif ( ! ( ( $hostname =~ /$RegExp{host}{relative}/o ) || ( $hostname =~ /$RegExp{host}{absolute}/o ) ) ) {
      $Error = "Invalid referenced hostname found in ";
      $Fatal = 1;
      }
    $CacheCheckRefHostError { $hostname } = $Error;
    $CacheCheckRefHostFatal { $hostname } = $Fatal;
    if ( $Error ne "" ) {
      $Error = $Error . $what;
      }
    }

return $Error, $Fatal;
}

sub CheckHost {
  my ( $hostname ) = uc shift @_;
  my ( $domain ) = uc shift @_;

  my ( @chars );

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  if ( $hostname =~ /@/o ) {
    $hostname = $domain;
    }

  if ( defined $CacheCheckHostError { $hostname } ) {
    if ( $CacheCheckHostError { $hostname } ne "" ) {
      $Error = $CacheCheckHostError { $hostname };
      $Fatal = $CacheCheckHostFatal { $hostname };
      }
    }
  else {
    $_ = $hostname;
    s/$ValidChars//og;
    s/\*//og;
    s/\///og;
  
    if ( length ( $_ ) > 0 ) {
      $Error = "Illegal characters found";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /\.\./o ) {
      $Error = "Double \".\" in first field";
      $Fatal = 1;
      }
    elsif ( $hostname =~ /^\./o ) {
      $Error = "Hostnames cannot start with a \".\"";
      $Fatal = 1;
      }
    elsif ( ! ( ( $hostname =~ /$RegExp{host}{relative}/o ) || ( $hostname =~ /$RegExp{host}{absolute}/o ) ) ) {
      $Error = "Invalid hostname";
      $Fatal = 1;
      }
    $CacheCheckHostError { $hostname } = $Error;
    $CacheCheckHostFatal { $hostname } = $Fatal;
    }
  return $Error, $Fatal;
  }

sub CheckTTL {
  my ( $ttl ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  if ( ( $ttl ne "" ) && ( $ttl < 10 ) ) {
    $Error = "TTL to low";
    }

  return $Error, $Fatal;
  }

sub NormalizeInput {
  my ( $InputLine ) = shift @_;

  $_ = $InputLine;

  if ( $_ =~ /\"/o ) {
    s/^\s+(.+\".*)$/ $1/o;
    s/^(.+?)\s+(.*)$/$1 $2/o;
    s/^(.+ .+?)\s+(.*)$/$1 $2/o;
    s/^(.+ .+ .+?)\s+(.*)$/$1 $2/o;
    s/^(.+ .+ .+ .+?)\s+(.*)$/$1 $2/o;
    }

  else {
    s/\s+/ /og;
    }
  s/^\$INCLUDE.*//o;
  s/^\$GENERATE.*//o;
  s/;.*$//o;
  s/\s+$//og;

  return uc $_;
  }

sub NormalizeInput2 {
  my ( $InputLine ) = shift @_;

  my ( $one, $two );
  my ( @records );
  my ( $element );

  $_ = $InputLine;

  @_ = ();

  if ( $_ =~ /\"/o ) {
    /(.*?)(\".*)/o;
    $one = $1;
    $two = $2;
    @records = split /\s+/, $one;
    push @records, $two;
    }
  else {
    @records = split /\s+/, $_;
    }

  $element = shift @records;

  push @_,  $element;

  $element = shift @records;

  if ( $element =~ /^$RegExp{age}{numeric}|$RegExp{age}{alphanumeric}$/o ) {
    push @_, $element;
    $element = shift @records;
    }

  if ( ! defined $classes { $element } ) {
    push @_, "IN";
    }
  else {
    push @_, $element;
    $element = shift @records;
    }

  push @_, $element;

  foreach ( @records ) {
    push @_, $_;
    }

  $_ = join " ", @_;

  return uc $_;
  }

sub LogError {
  my ( $FileName ) = shift @_;
  my ( $Error ) = shift @_;
  my ( $Fatal ) = shift @_;
  my ( $number ) = shift @_;

  if ( $Fatal > 0 ) {
    print STDERR $FileName . ": line " . $number . ": syntax error: " . $Error . "\n";
    print STDERR $InputLine { $number } . "\n";
    }
  elsif ( ! ( defined $opt_s ) ) {
    print STDERR $FileName . ": line " . $number . ": warning: " . $Error . "\n";
    print STDERR $InputLine { $number } . "\n";
    }
}

sub LogErrors {
  my ( $FileName ) = shift @_;
  my ( $Error ) = shift @_;
  my ( $Fatal ) = shift @_;
  my ( @number ) = @_;

  my ( $ErrorNumbers ) = join ',', @number;

  if ( $Fatal > 0 ) {
    print STDERR $FileName . ": line " . $ErrorNumbers . ": syntax error: " . $Error . "\n";
    foreach $ErrorNumbers ( @number ) {
      print STDERR $InputLine { $ErrorNumbers } . "\n";
      }
    }
  elsif ( ! ( defined $opt_s ) ) {
    print STDERR $FileName . ": line " . $ErrorNumbers . ": warning: " . $Error . "\n";
    foreach $ErrorNumbers ( @number ) {
      print STDERR $InputLine { $ErrorNumbers } . "\n";
      }
    }
}

sub SplitDomain {
  my ( $Fqdn ) = shift @_;
  my ( $Domain ) = shift @_;

  my ( $Domain1 );
  my ( $prefix );

  my ( @Result );

  if ( ( $Fqdn eq $Domain ) | ( length ( $Domain ) >= length ( $Fqdn ) ) ) {
    push @Result, $Fqdn;
    }
  else {
    $Domain = "." . $Domain;
    $Domain1 = substr ( $Fqdn, ( length ( $Fqdn ) - length ( $Domain ) ) );
    if ( $Domain1 eq $Domain ) {
      $prefix = substr ( $Fqdn, 0, ( length ( $Fqdn ) - length ( $Domain ) ) ); 
      push @Result, ( $prefix . $Domain );
      while ( $prefix =~ /\./o ) {
        $_ = $prefix;
        /\S+?\.(\S+)/o;
        $prefix = $1;
        push @Result, ( $prefix . $Domain );
        }
      }
    else {
      push @Result, $Fqdn;
      }
    }

  return @Result;
}

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_A {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to NS";
    $Fatal = 1;
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv4}{full}/ ) {
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv4}{short}/ ) {
    $Error = "Short IP address found";
    $Fatal = 0;
    }
  else {
    $Error = "No valid IP address found";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "A" } = \ & check_A;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_A6 {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to A6";
    $Fatal = 1;
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv4}{full}|$RegExp{IPv4}{short}/o ) {
    $Error = "IPv4 address found where IPv6 address expected";
    $Fatal = 1;
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv6}{full}|$RegExp{IPv6}{short}/o ) {
    }
  else {
    $Error = "No valid IPv6 address found";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "A6" } = \ & check_A6;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_AAAA {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to AAAA";
    $Fatal = 1;
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv4}{full}|$RegExp{IPv4}{short}/o ) {
    $Error = "IPv4 address found where IPv6 address expected";
    $Fatal = 1;
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv6}{full}|$RegExp{IPv6}{short}/o ) {
    }
  else {
    $Error = "No valid IPv6 address found";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "AAAA" } = \ & check_AAAA;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_CERT {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  my ( $count ) = $rest =~ tr/"/"/;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) < 4 ) {
    $Error = "At least four fields required as option to CERT";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "CERT" } = \ & check_CERT;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_CNAME {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to CNAME";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 0 ], "referenced hostname" );
    }

  return $Error, $Fatal;
  }

$InitCalls { "CNAME" } = \ & check_CNAME;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_HINFO {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) > 2 ) {
    if ( $rest =~ /^\".*?\"\s+\".*?"$/o ) {
      /(\".*?\")\s+(\".*?")/;
      @list = ( $1, $2 );
      }
    elsif ( $rest =~ /^\".*?\"\s+\w+$/o ) {
      /(\".*?\")\s+(\w+)/;
      @list = ( $1, $2 );
      }
    elsif ( $rest =~ /^\w+\s+\".*?\"$/o ) {
      /(\w+)\s+(\".*?\")/;
      @list = ( $1, $2 );
      }
    else {
      $Error = "Only two fields allowed as option to HINFO";
      $Fatal = 1;
      }
    }
  elsif ( scalar ( @list ) < 2 ) {
    $Error = "Two fields required as option to HINFO";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "HINFO" } = \ & check_HINFO;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_KEY {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  my ( $flag );
  my ( $protocol );
  my ( $algorithm );
  my ( $public_key );

  my ( $flag_num );
  my ( $flag_error );

  my ( %protocols ) = ( "NONE"   => 0,
                        "TLS"    => 1,
                        "EMAIL"  => 2,
                        "DNSSEC" => 3,
                        "IPSEC"  => 4,
                        "ALL"    => 255,
                        0        => "NONE",
                        1        => "TLS",
                        2        => "EMAIL",
                        3        => "DNSSEC",
                        4        => "IPSEC",
                        255      => "ALL" );

  my ( %algorithms ) = ( "RSAMD5"    => 1,
                         "DH"        => 2,
                         "DSA"       => 3,
                         "ECC"       => 4,
                         "INDIRECT"  => 252,
                         "PRIVATEDNS" => 253,
                         "PRIVATEOID" => 254 );

  my ( %flags ) = ( "NOCONF" => 1,
                    "NOAUTH" => 2,
                    "NOKEY"  => 3,
                    "FLAG2"  => 4,
                    "EXTEND" => 8,
                    "FLAG4"  => 16,
                    "FLAG5"  => 32,
                    "USER"   => 0,
                    "ZONE"   => 64,
                    "HOST"   => 192,
                    "NTYP3"  => 0,
                    "FLAG8"  => 256,
                    "FLAG9"  => 512,
                    "FLAG10" => 1024,
                    "FLAG11" => 2048,
                    "SIG0"   => 0,
                    "SIG1"   => 4096,
                    "SIG2"   => 8192,
                    "SIG3"   => 12288,
                    "SIG4"   => 16384,
                    "SIG5"   => 20480,
                    "SIG6"   => 24576,
                    "SIG7"   => 28672,
                    "SIG8"   => 32768,
                    "SIG9"   => 36864,
                    "SIG10"  => 40960,
                    "SIG11"  => 45056,
                    "SIG12"  => 49152,
                    "SIG13"  => 53248,
                    "SIG14"  => 57344,
                    "SIG15"  => 61440 );

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) < 4 ) {
    $Error = "At least four fields required as option to KEY";
    $Fatal = 1;
    }
  else {

    $flag = uc ( shift @list );
    $protocol = uc ( shift @list );
    $algorithm = uc ( shift @list );
    $public_key = uc ( join ( '', @list ) );

    if ( $flag =~ /^0X\d{1,}$/o ) {
      $flag_num = hex ( lc ( $flag ) );
      }
    elsif ( $flag =~ /^\d{1,}$/o ) {
      $flag_num = $flag;
      }
    else {
      @list = split ( $flag, /|/o );
      $flag_num = 0;
      foreach ( @list ) {
        if ( $flag_error == 0 ) {
          if ( defined $flags { $_ } ) {
            $flag_num = $flag_num | $flags { $_ };
            }
          else {
            $flag_error = 1;
            }
          }
        }
      }

    if ( ! ( ( $protocol =~ /^\d{1,3}$/o ) | ( defined $protocols { $protocol } ) ) ) {
      $Error = "Invalid protocol";
      $Fatal = 1;
      }
    elsif ( ! ( ( $algorithm =~ /^\d{1,3}$/o ) | ( defined $algorithms { $protocol } ) ) ) {
      $Error = "Invalid algorithm";
      $Fatal = 1;
      }
    elsif ( $flag_error != 0 ) {
      $Error = "Invalid FLAGS";
      $Fatal = 1;
      }
    elsif ( $flag_num >= 65536 ) {
      $Error = "There are only 16 bits in FLAGS";
      $Fatal = 1;
      }
    elsif ( ! ( ( $protocol == 3 ) || ( $protocol eq "DNSSEC" ) ) ) {
      $Error = "The KEY RR SHOULD only be used for DNSSEC. (See also RFC 3445) Please use the CERT RR instead.";
      $Fatal = 0;
      }
    }

  return $Error, $Fatal;
  }

$InitCalls { "KEY" } = \ & check_KEY;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

#
# LOC RR is described in RFC1876

sub check_LOC {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  my ( $d1, $m1, $s1, $d2, $m2, $s2 );
  my ( $lat, $lon );
  my ( $alt, $siz, $hp, $vp );

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( $rest =~ /^\d+(\s+\d+(\s+\d+(\.\d+){0,1}){0,1}){0,1}\s+[NS]\s+\d+(\s+\d+(\s+\d+(\.\d+){0,1}){0,1}){0,1}\s+[EW]\s+-{0,1}\d+(\.\d{1,2}){0,1}M{0,1}(\s+\d+(\.\d{1,2}){0,1}M{0,1}(\s+\d+(\.\d{1,2}){0,1}M{0,1}(\s+\d+(\.\d{1,2}){0,1}M{0,1}){0,1}){0,1}){0,1}$/o ) {
  /^(\d+)(\s+(\d+)(\s+(\d+(\.\d+){0,1})){0,1}){0,1}\s+([NS])\s+(\d+)(\s+(\d+)(\s+(\d+(\.\d+){0,1})){0,1}){0,1}\s+([EW])\s+(-{0,1}\d+(\.\d{1,2}){0,1})M{0,1}(\s+(\d+(\.\d{1,2}){0,1})M{0,1}(\s+(\d+(\.\d{1,2}){0,1})M{0,1}(\s+(\d+(\.\d{1,2}){0,1})M{0,1}){0,1}){0,1}){0,1}$/o;
    $d1 = $1;
    $m1 = $2;
    $s1 = $3;
    $lat = $4;
    $d2 = $5;
    $m2 = $6;
    $s2 = $7;
    $lon = $8;
    $alt = $9;
    $siz = $10;
    $hp = $11;
    $vp = $12;
    if ( $m1 eq "" ) { $m1 = 0; }
    if ( $s1 eq "" ) { $s1 = 0; }
    if ( $m2 eq "" ) { $m2 = 0; }
    if ( $s2 eq "" ) { $s2 = 0; }
    if ( $alt eq "" ) { $alt = 0; }
    if ( $siz eq "" ) { $siz = 0; }
    if ( $hp eq "" ) { $hp = 0; }
    if ( $vp eq "" ) { $vp = 0; }
    }
  else {
    $Error = "Invalid format";
    $Fatal = 1;
    }
  return $Error, $Fatal;
  }

$InitCalls { "LOC" } = \ & check_LOC;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_MX {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 2 ) {
    $Error = "Two fields required as option to MX";
    $Fatal = 1;
    }
  elsif ( ! ( $list [ 0 ] =~ /^\d+$/o ) ) {
    $Error = "Invalid priority";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 1 ], "referenced hostname" );
    }

  return $Error, $Fatal;
  }

$InitCalls { "MX" } = \ & check_MX;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_NS {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to NS";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 0 ], "referenced hostname" );
    }

  return $Error, $Fatal;
  }

$InitCalls { "NS" } = \ & check_NS;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_NSAP {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to NSAP";
    $Fatal = 1;
    }
  elsif ( $rest =~ /^[\da-fA-F]{1,2}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,2}$/o ) {
    $Error = "Missing \"0x\" at start of NSAP address";
    $Fatal = 0;
    }
  elsif ( $rest =~ /^0x[\da-fA-F]{1,2}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,4}\.[\da-fA-F]{1,2}$/o ) {
    }
  else {
    $Error = "Possible invalid NSAP address";
    $Fatal = 0;
    }

  return $Error, $Fatal;
  }

$InitCalls { "NSAP" } = \ & check_NSAP;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_NXT {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "NXT" } = \ & check_NXT;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_PTR {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal );

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 1 ) {
    $Error = "One field required as option to PTR";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 0 ], "referenced hostname" );
    }

  return $Error, $Fatal;
  }

$InitCalls { "PTR" } = \ & check_PTR;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_RP {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 2 ) {
    $Error = "Two fields required as option to RP";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 0 ], "referenced mailbox" );
    if ( $Error eq "" ) {
      ( $Error, $Fatal ) = CheckRefHost ( $list [ 1 ], "referenced domain" );
      }
    }

  return $Error, $Fatal;
  }

$InitCalls { "RP" } = \ & check_RP;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_SIG {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "SIG" } = \ & check_SIG;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_SOA {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  $_ = $list [ 2 ];
  s/^0+//;
  if ( $GlobalSerial eq -1 ) {
    $GlobalSerial = $_;
    }

  if ( defined $opt_S ) {
    print $GlobalSerial . "\n";
    exit;
    }

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 7 ) {
    $Error = "Invalid number of options in SOA";
    $Fatal = 1;
    }
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 0 ], "primary server" );
    if ( $Error eq "" ) {
      ( $Error, $Fatal ) = CheckRefHost ( $list [ 1 ], "contact information" );
      if ( $Error eq "" ) {
        if ( ! ( $list [ 2 ] =~ /^\d+$/ ) ) {
          $Error = "Serial is not numeric in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 3 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Refresh is not valid in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 4 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Retry is not valid in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 5 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Expire is not valid in SOA";
          $Fatal = 1;
          }
        elsif ( ! ( $list [ 6 ] =~ /^($RegExp{age}{numeric}|$RegExp{age}{alphanumeric})$/ ) ) {
          $Error = "Minimum TTL is not valid in SOA";
          $Fatal = 1;
          }
#        else {
#          if ( $list [ 3 ] != 28800 ) {
#            $Error = "Reresh is not the recomended 28800 (8 hours)";
#            }
#          if ( $list [ 4 ] != 7200 ) {
#            $Error = "Retry is not the recomended 7200 (2 hours)";
#            }
#          if ( $list [ 5 ] != 604800 ) {
#            $Error = "Expire is not the recomended 604800 (7 days)";
#            }
#          if ( $list [ 6 ] != 86400 ) {
#            $Error = "Minimum is not the recomended 86400 (1 day)";
#            }
#          }
        else {
          $list [ 3 ] = ConvertAge ( uc $list [ 3 ] ) ;
          $list [ 4 ] = ConvertAge ( uc $list [ 4 ] );
          $list [ 5 ] = ConvertAge ( uc $list [ 5 ] );
          $list [ 6 ] = ConvertAge ( uc $list [ 6 ] );
          if ( $list [ 3 ] < $SOA_Refresh_Min ) {
            $Error = "Refresh is smaller then the recomended " . $SOA_Refresh_Min;
            }
          if ( $list [ 3 ] > $SOA_Refresh_Max ) {
            $Error = "Refresh is larger then the recomended " . $SOA_Refresh_Max;
            }
          elsif ( $list [ 4 ] < $SOA_Retry_Min ) {
            $Error = "Retry is smaller then the recomended " . $SOA_Retry_Min;
            }
          elsif ( $list [ 4 ] > $SOA_Retry_Max ) {
            $Error = "Retry is larger then the recomended " . $SOA_Retry_Max;
            }
          elsif ( $list [ 5 ] < $SOA_Expire_Min ) {
            $Error = "Expire is smaller then the recomended " . $SOA_Expire_Min;
            }
          elsif ( $list [ 5 ] > $SOA_Expire_Max ) {
            $Error = "Expire is larger then the recomended " . $SOA_Expire_Max;
            }
          elsif ( $TTLbyDirective == -1 ) {
            if ( $list [ 6 ] < $SOA_Minimum_Min ) {
              $Error = "Minimum is smaller then the recomended " . $SOA_Minimum_Min;
              }
            elsif ( $list [ 6 ] > $SOA_Minimum_Max ) {
              $Error = "Minimum is greater then the recomended " . $SOA_Minimum_Min;
              }
            }
          elsif ( $TTLbyDirective != -1 ) {
            if ( $list [ 6 ] < $SOA_Minimum_Negative_Min ) {
              $Error = "Minimum is smaller then the recomended " . $SOA_Minimum_Negative_Min;
              }
            elsif ( $list [ 6 ] > $SOA_Minimum_Negative_Max ) {
              $Error = "Minimum is greater then the recomended " . $SOA_Minimum_Negative_Max;
              }
            }
          }
        }
      }
    }

  return $Error, $Fatal;
  }

$InitCalls { "SOA" } = \ & check_SOA;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_SRV {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) != 4 ) {
    $Error = "Four fields required as option to SRV";
    $Fatal = 1;
    }
  elsif ( ! ( $list [ 0 ] =~ /^\d+$/ ) ) {
    $Error = "Invalid priority";
    $Fatal = 1;
    }
  elsif ( ! ( $list [ 1 ] =~ /^\d+$/ ) ) {
    $Error = "Invalid weight";
    $Fatal = 1;
    }
  elsif ( ! ( $list [ 2  ] =~ /^\d+$/ ) ) {
    $Error = "Invalid port";
    $Fatal = 1;
    }   
  else {
    ( $Error, $Fatal ) = CheckRefHost ( $list [ 3 ], "referenced hostname" );
    }

  return $Error, $Fatal;
  }

$InitCalls { "SRV" } = \ & check_SRV;
#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_TXT {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( $count ) = $rest =~ tr/"/"/;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( ! ( $rest =~ /^\".*\"$/o ) ) {
    $Error = "TXT record should be contained in \"'s";
    $Fatal = 0;
    }
#  elsif ( $rest =~ /^\".*\".*\"$/o ) {
#    $Error = "Extra \"'s found in TXT record";
#    $Fatal = 1;
#    }
  elsif ( ( $count / 2 ) != int ( $count / 2 ) ) {
    $Error = "TXT record contains an odd number of \"'s";
    $Fatal = 1;
    }
  elsif ( length ( $rest ) > 512 ) {
    $Error = "TXT record to long";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "TXT" } = \ & check_TXT;

#  Copyright 2001 Anne Marcel Roorda. All rights reserved. 
#
#  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
#
#      1.Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
#      2.Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution. 
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
#  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

sub check_WKS {
  my ( $GlobalTTL ) = shift @_;
  my ( $host ) = shift @_;
  my ( $class ) = shift @_;
  my ( $ttl ) = shift @_;
  my ( $rr ) = shift @_;
  my ( $rest ) = shift @_;

  my ( $Error ) = "";
  my ( $Fatal ) = 0;

  my ( @list ) = split /\s+/, $rest;

  if ( $class ne "IN" ) {
    $Error = "Invalid CLASS for RR";
    $Fatal = 1;
    }
  elsif ( scalar ( @list ) <= 2 ) {
    $Error = "A minimum of three fields are required as option to WKS";
    $Fatal = 1;
    }
  elsif ( $list [ 0 ] =~ /$RegExp{IPv4}{short}/o ) {
    $Error = "Short IP address found";
    $Fatal = 0;
    }
  elsif ( ! ( $list [ 0 ] =~ /$RegExp{IPv4}{full}/o ) ) {
    $Error = "No valid IP address found";
    $Fatal = 1;
    }

  return $Error, $Fatal;
  }

$InitCalls { "WKS" } = \ & check_WKS;

#
# We do need to know what file to load

my ( $FileName );

$FileName = $ARGV[0];

if ( ! defined $FileName ) {
  print "usage: zonecheck [-c cachezise] [-d domainname] [-h <refresh_min>,<refresh_max>,<retry_min>,<retry_max>,<expire_min>,<expire_max>,<minimum_min>,<minimum_max>,<minimum_negative_min>,<minimum_negative_max>] [-msSV] zonefile\n";
  exit 1;
  }

#
# If no domain name is set on the command line
# then we use the filename as domain name

if ( $domain eq "" ) {
  $_ = (fileparse($FileName))[0];
  $domain = $_;
  }

#
# The domain should end in "."

if ( ! ( $domain =~ /\.$/o ) ) {
  $domain .= ".";
  }

$GlobalDomain = uc ( $domain );

# $DomainList { $domain } = $domain;

open INPUT, "$ARGV[0]" or die $FileName . ": No such file or directory\n";

#
# Taking the input lines, and parsing them into a standard format
# is split up in 2 routines.
#
# NormalizeInput: Takes an input line, and snips off any unneeded
# whitespace. It also snips comments and "$INCLUDE" and "$GENERATE"
# statements.
#
# NormalizeInput2: Makes sure that all elements are present. This
# means it now has a TTL, CLASS, RR, and options to the RR.
#
# Once both have been completed we are left with a standardized
# input line, and we can now disect it and pass it to the required
# RR check.
#
# BEWARE: Each RR check can _ONLY_ return 1 error / warning, and
# the first _FATAL_ error should immediately return.
#
# We also keep track of the various RR's found for the host entries
# so we can do some smarter checks later on.

$number = 0;

MainLoop: while ( <INPUT> ) {
  $number++;
  chomp;
  $Input = $_;
  $Line = NormalizeInput ( $Input );

  $InputLine { $number } = $Input;

  if ( $Line eq "" ) {
    delete $InputLine { $number };
    next MainLoop;
    }
  else {
    if ( $Line =~ /\(/o ) {
      $number2 = $number;
      $Buffer = $Line;
      next MainLoop if ( ! ( $Buffer =~ /\)/o ) );
      }
    elsif ( ( $Buffer =~ /\(/o ) && ( ! ( $Buffer =~ /\)/o ) ) ) {
      $Buffer .= " " . $Line;
      $number_of_lines_joined++;
      next MainLoop if ( ! ( $Buffer =~ /\)/o ) );
      $_ = $Buffer;
      s/[\(\)]//og;
      $Buffer = $_;
      }
    else {
      $number2 = $number;
      $Buffer = $Line
      }
    $number = $number2;

    $_ = $Buffer;

    if ( $_ =~ /^\$ORIGIN/o ) {
      /^\$ORIGIN\s+(\S+)/o;
      $domain = $1;
      $number += $number_of_lines_joined;
      $number_of_lines_joined = 0;
      next MainLoop;
      }

    if ( $_ =~ /^\$TTL\s+\S+$/o ) {
      /^\$TTL\s+(\S+)$/o;
      if ( defined ConvertAge ( uc $1 ) ) {
        $TTLbyDirective = ConvertAge ( uc $1 );
        }
      $number += $number_of_lines_joined;
      $number_of_lines_joined = 0;
      next MainLoop;
      }

    if ( defined $opt_v ) {
      print "---" . $number . "- -" . $_ . "\n";
      }

    $Buffer = NormalizeInput2 ( $Buffer );
    $_ = $Buffer;

    if ( defined $opt_v ) {
      print "- -" . $number . "- -" . $_ . "\n";
      }

    if ( $_ =~ /^(\S+)\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $2;
      $ttl = ConvertAge ( uc $3 );
      $rr = $5;
      $rest = $6;
      if ( defined $opt_v ) {
        print "* 1 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $2;
      $ttl = $3;
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 2 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o ) {
      /^\s+(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(.*)/o;
      $class = $1;
      $ttl = ConvertAge ( uc $2 );
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 3 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o ) {
      /^\s+(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(.*)/o;
      $class = $1;
      $ttl = uc $2;
      $rr = $3;
      $rest = $4;
      if ( defined $opt_v ) {
        print "* 4 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $4;
      $ttl = ConvertAge ( uc $2 );
      $rr = $5;
      $rest = $6;
      if ( defined $opt_v ) {
        print "* 5 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $3;
      $ttl = $2;
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 6 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^\s+($RegExp{age}{alphanumeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $class = $3;
      $ttl = ConvertAge ( uc $1 );
      $rr = $4;
      $rest = $5;
      if ( defined $opt_v ) {
        print "* 7 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^\s+($RegExp{age}{numeric})\s+(\S+)\s+(\S+)\s+(.*)/o;
      $class = $2;
      $ttl = $1;
      $rr = $3;
      $rest = $4;
      if ( defined $opt_v ) {
        print "* 8 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^(\S+)\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^(\S+)\s+(\S+)\s+(\S+)\s+(.*)/o;
      $host = $1;
      $class = $2;
      $ttl = "";
      $rr = $3;
      $rest = $4;
      if ( defined $opt_v ) {
        print "* 9 -" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    elsif ( $_ =~ /^\s+(\S+)\s+(\S+)\s+(.*)/o ) {
      /^\s+(\S+)\s+(\S+)\s+(.*)/o;
      $class = $1;
      $ttl = "";
      $rr = $2;
      $rest = $3;
      if ( defined $opt_v ) {
        print "* 10-" . $host . "-" . $ttl . "-" . $class . "-" . $rr . "-" . $rest . "\n";
        }
      }
    else {
      if ( defined $opt_v ) {
        print "* 11-" . $_ . "\n";
        }
      LogError ( $FileName, "Invalid input", 1, $number );
      $ExitCode = 1;
      $number += $number_of_lines_joined;
      $number_of_lines_joined = 0;
      next MainLoop;
      }

    if ( ! defined $InitCalls { $rr } ) {
      if ( defined $opt_v ) {
        print "+ -" . $rr . "-\n";
        }
      LogError ( $FileName, "Unknown RR found", 1, $number );
      $ExitCode = 1;
      }
    else {
      ( $Error, $Fatal ) = CheckHost ( $host, $domain );
      if ( $Error ne "" ) {
        LogError ( $FileName, $Error, $Fatal, $number );
        $ExitCode = 1 if ( $Fatal > 0 );
        }
      else {
        ( $Error, $Fatal ) = CheckTTL ( $ttl );
        if ( $Error ne "" ) {
          LogError ( $FileName, $Error, $Fatal, $number );
          $ExitCode = 1 if ( $Fatal > 0 );
          }
        else {
          if ( $host ne "@" ) {
            if ( $host =~ /\.$/o ) {
              $fqdn = uc ( $host );
              }
            else {
              $fqdn = uc ( $host . "." . $domain );
              }
            $_ = $fqdn;
            /(\S+?)\.(\S+)/o;
            $prefix = $1;
            $suffix = $2;
            }
          else {
            $fqdn = uc ( $domain );
            $_ = $fqdn;
            /(\S+?)\.(\S+)/o;
            $prefix = $1;
            $suffix = $2;
            }

          if ( defined $Lines { $fqdn } ) {
            $Lines { $fqdn } = $Lines { $fqdn } . " " . $number;
            }
          else {
            $Lines { $fqdn } = $number;
            }

          if ( defined $Records { $fqdn } ) {
            $Records { $fqdn } .= " " . $rr;
            }
          else {
            $Records { $fqdn } = $rr;
            }

          $_ = $InitCalls { $rr };

          ( $Error, $Fatal ) = & $_ ( $GlobalTTL, $host, $class, $ttl, $rr, $rest );

          if ( $Error ne "" ) {
            LogError ( $FileName, $Error, $Fatal, $number );
            $ExitCode = 1 if ( $Fatal > 0 );
            }

          if ( ( $host ne "@" ) && ( $host ne $GlobalDomain ) ) {
            if ( ( $rr eq "NS" ) || ( $rr eq "SOA" ) ) {
              $DomainList { $fqdn } = $fqdn;
              }

            $domain1 = "." . $GlobalDomain;

            if ( ! ( defined $DomainList { $fqdn } ) ) {
              if ( length ( $domain1 ) < length ( $fqdn ) ) {
                $domain2 = substr ( $fqdn, ( length ( $fqdn ) - length ( $domain1 ) ) );
                if ( $domain2 ne $domain1 ) {
                  $DomainList { $fqdn } = $fqdn;
                  }
                }
              }

            foreach ( SplitDomain ( $fqdn, $GlobalDomain ) ) {
              if ( defined $Fqdn { $_ } ) {
                $Fqdn { $_ } .= " " . $number;
                }
              else {
                $Fqdn { $_ } = $number;
                }
              }

            $Fqdn_host { $number } = $host;
            $Fqdn_domain { $number } = $domain;
            $Fqdn_rr { $number } = $rr;
            }
          }
        }
      }
    }
  $number += $number_of_lines_joined;
  $number_of_lines_joined = 0;
  }

#
# The following tie's can now be removed, they are no longer required
# and the memory and disk space can probebly be used better elsewhere

untie %CacheCheckRefHostError;
untie %CacheCheckRefHostFatal;

untie %CacheCheckHostError;
untie %CacheCheckHostFatal;

$Error = "";
$Fatal = 0;

#
# The first big NONO... Not having a SOA record is FATAL

if ( ! ( $Records { $GlobalDomain } =~ /SOA/ ) ) {
  LogError ( $FileName, "No SOA found for domain " . $GlobalDomain, 1, 1);
  $ExitCode = 1;
  }

#
# Because we kept track of the various RR's found for the hosts
# we can now run through them :)

foreach $fqdn ( keys %Records ) {
  undef %rr_list;
  undef %allowed_rr;
  foreach $rr ( split ( /\s+/, $Records { $fqdn } ) ) {
#    if ( ( $rr ne "SIG" ) && ( $rr ne "KEY" ) && ( $rr ne "NXT" ) ) {
      if ( defined $rr_list { $rr } ) {
        $rr_list { $rr } ++;
        }
      else {
        $rr_list { $rr } = 1;
        }
#      }
    }

  foreach $rr ( keys %rr_list ) {
    $allowed_rr { $rr } = $rr_list { $rr };
    }

#
# SIG, KEY, and NXT may exist with _any_ other RR, so to avoid confusion
# we pretend to have never seen them. Any checks for these records should 
# have been done before this point.

  if ( defined $allowed_rr { "SIG" } ) {
    delete $allowed_rr { "SIG" };
    }
  if ( defined $allowed_rr { "KEY" } ) {
    delete $allowed_rr { "KEY" };
    }
  if ( defined $allowed_rr { "NXT" } ) {
    delete $allowed_rr { "NXT" };
    }

#
# Check number of NS records

  if ( defined $rr_list { "NS" } ) {
    if ( $rr_list { "NS" } == 1 ) {
      $Error = "Only one NS record found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    if ( defined $rr_list { "SOA" } ) {

#
# If a SOA RR also exists, then we also allow the following RR's

#
# Nothing to check, all other checks done elsewhere

#
# If ever a RR is defined that is not legal with a SOA RR then
# this is where the check should go

      }
    else {

#
# No SOA RR exists, so no extra RR's allowed

      if ( scalar keys %allowed_rr > 1 ) { 
        if ( defined $opt_v ) {
          print "NOSOA -";
          print scalar keys %allowed_rr;
          print "-\n";
          foreach $line ( keys %allowed_rr ) {
            print "NOSOA +" . $line . "+" . $allowed_rr { $line } . "+\n";
            }
          }
        $Error = "NS record, and other RR's found for \"" . lc $fqdn . "\"";
        $Fatal = 1;
        @_ = split /\s+/,  $Lines { $fqdn };
        LogErrors ( $FileName, $Error, $Fatal, @_ );
        $ExitCode = 1 if ( $Fatal > 0 );
        $Error = "";
        $Fatal = 0;
        }
      }

    }

#
# End of NS RR check

#
# LOC RR check

  if ( defined $rr_list { "LOC" } ) {

    if ( $rr_list { "LOC" } > 1 ) {
      $Error = "Multiple LOC RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of LOC RR check

#
# MX RR check

  if ( defined $rr_list { "MX" } ) {

    if ( $rr_list { "MX" } < 2 ) {
      $Error = "Only 1 MX RR found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of MX RR check

#
# CNAME RR check

  if ( defined $rr_list { "CNAME" } ) {

    if ( scalar keys %allowed_rr > 1 ) {
      $Error = "CNAME record, and other RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 1;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    if ( $rr_list { "CNAME" } > 1 ) {
      $Error = "Multiple CNAME RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 1;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of CNAME RR check

#
# PTR RR check

  if ( defined $rr_list { "PTR" } ) {

    if ( $rr_list { "PTR" } > 1 ) {
      $Error = "Multiple PTR RR's found for \"" . lc $fqdn . "\"";
      $Fatal = 0;
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }

    }

#
# End of PTR RR check

#
# Check for "*" records.

  if ( $fqdn =~ /\*/o ) {
    foreach ( keys %rr_list ) {
      if ( $_ eq "A" ) {
        $Error = "The use of * A records is actively discouraged";
        $Fatal = 0;
        }
      }
    if ( $Error ne "" ) { 
      @_ = split /\s+/,  $Lines { $fqdn };
      LogErrors ( $FileName, $Error, $Fatal, @_ );
      $ExitCode = 1 if ( $Fatal > 0 );
      $Error = "";
      $Fatal = 0;
      }
    }


#
# End of "*" record check

  }

untie %Lines;
untie %Records;

foreach $domain ( keys %DomainList ) {
  if ( defined $Fqdn { $domain } ) {
    foreach $number ( split ( /\s+/, $Fqdn { $domain } ) ) {
      $fqdn = $Fqdn_host { $number } . "." . $Fqdn_domain { $number };
      if ( $Fqdn_rr { $number } eq "A" ) {
        $Error = "glue found for host \"" . $Fqdn_host { $number } . "\" in domain \"" . $Fqdn_domain { $number } . "\"";
        $Fatal = 0;
        LogError ( $FileName, $Error, $Fatal, $number );
        }
      elsif ( $Fqdn_rr { $number } eq "AAAA" ) {
        $Error = "glue found for host \"" . $Fqdn_host { $number } . "\" in domain \"" . $Fqdn_domain { $number } . "\"";
        $Fatal = 0;
        LogError ( $FileName, $Error, $Fatal, $number );
        }
      elsif ( $Fqdn_rr { $number } eq "SOA" ) {
        }
      elsif ( $Fqdn_rr { $number } eq "NS" ) {
        }
      else {
        $Error = "non-glue record for host \"" . $Fqdn_host { $number } . "\" below bottom of zone in domain \"" . $Fqdn_domain { $number } . "\"";
        $Fatal = 1;
        LogError ( $FileName, $Error, $Fatal, $number );
        $ExitCode = 1;
        }
      }
    }
  }

if ( defined $opt_s ) {
  print $GlobalSerial . "\n";
  }

exit $ExitCode;
